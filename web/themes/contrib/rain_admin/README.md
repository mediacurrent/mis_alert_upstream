# Rain Admin Theme
The Rain Admin theme uses the Thunder Admin as a base with some color overrides and other small modifications.

# Updating CSS
All the CSS files in css/ outside of "hacks.css" are generated from the Thunder Admin theme using "npm run build."
These files were created after modifying color variables in thunder_admin/sass-includes/variables.scss. These files
should not be edited directly. Instead for overrides use the hacks.css file to make additional changes to the look
and feel. This will keep this theme layer lightweight.

## Current variable overrides.
/* stylelint-disable plugin/no-browser-hacks */
$primary-color: #0C5C7F;
$primary-color-intense: #0f739f;
$primary-color-dark: #09364A;
$primary-color-darker: #062330;
$primary-color-darkest: #031218;
$primary-color-light: #EAF6FE;
$primary-color-lighter-2: #D8D8D8;
$primary-color-lighter: #f2f2f2;
$primary-color-lightest: #fff;

$font-family: 'Roboto', sans-serif;

## Update image paths
- Change '(../../images/' to '(../../../thunder_admin/images/' if in components subfolder
- Change '(/../images/' to '(../../thunder_admin/images/' if in main css subfolder
- Change '../icons/' to '../../thunder_admin/images/icons/' in paragraphs-widget.css