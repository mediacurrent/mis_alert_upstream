## Rain Paragraphs
Contains common Drupal paragraph types and fields.

- Block: Gives editors the ability to associate a system block.
- Breaker: A full width card featuring or highlighting a piece of content.
- Card: A teaser to a piece of content, local or external.
- Card List: Gives editors a way to feature a list of content. Utilizes the card paragraph.
- Columns: Allows editors to embed content into vertical columns.
- Embed: Allow script embeds to be easily added to the page.
- FAQ: Question and Answer pairs.
- Form: Webform integration.
- Gallery: Media gallery that can include video and images.
- Gallery Carousel: Spotlight images and content in a rotating carousel with thumb navigation.
- Hero Media: Promotional teasers that include a large image. Often used with a carousel plugin.
- Map: Place multiple pins on a Google Map.
- Media: Place an media asset on the page.
- Page title: Adds a paragraph / field that allows editors to move the placement of the page title.
- Quote: Author quote.
- Text: Simple HTML text field for entering copy.
- Views: Allows editors to reference a views block. A "recent content" view is provided by default.
