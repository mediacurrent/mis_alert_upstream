This file was automatically generated by Composer Patches (https://github.com/cweagans/composer-patches)
Patches applied to this directory:

2983448: Remove setting of field_last_password_reset on install
Source: https://www.drupal.org/files/issues/2020-03-23/Do_not_set_user_fields_on_install-2983448-17.patch


