<?php

/**
 * @file
 * Enables modules and site configuration for the COVID-19 Alert Demo profile.
 */

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_preprocess_page_title().
 */
function alert_preprocess_page_title(&$variables) {
  $node = \Drupal::request()->attributes->get('node');

  // Unset the title if the "Exclude title" checkbox is checked.
  if (!empty($node) && $node->hasField('field_exclude_title') && !empty($node->get('field_exclude_title')->value)) {
    $variables['title'] = '';
  }
}
