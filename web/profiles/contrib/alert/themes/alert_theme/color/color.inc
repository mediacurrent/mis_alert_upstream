<?php

/**
 * @file
 * Lists available colors and color schemes for the collective theme.
 */

$info = [
  // Available colors and color labels used in theme.
  'fields' => [
    'top' => t('Header background top'),
    'base' => t('Main background'),
    'text' => t('Secondary color'),
    'link' => t('Link color'),
  ],
  // Pre-defined color schemes.
  'schemes' => [
    'default' => [
      'title' => t('Default'),
      'colors' => [
        'top' => '#152f52',
        'base' => '#ffffff',
        'text' => '#000000',
        'link' => '#0d5c9f',
      ],
    ],
    'firehouse' => [
      'title' => t('Firehouse'),
      'colors' => [
        'top' => '#cd2d2d',
        'base' => '#ffffff',
        'text' => '#888888',
        'link' => '#7c0b13',
      ],
    ],
    'ice' => [
      'title' => t('Ice'),
      'colors' => [
        'top' => '#333333',
        'base' => '#ffffff',
        'text' => '#4a4a4a',
        'link' => '#019dbf',
      ],
    ],
    'plum' => [
      'title' => t('Plum'),
      'colors' => [
        'top' => '#4c1c58',
        'base' => '#fffdf7',
        'text' => '#301313',
        'link' => '#9d408d',
      ],
    ],
    'slate' => [
      'title' => t('Slate'),
      'colors' => [
        'top' => '#4a4a4a',
        'base' => '#ffffff',
        'text' => '#3b3b3b',
        'link' => '#0073b6',
      ],
    ],
  ],

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => [
    'dist/css/accordion.css',
    'dist/css/breadcrumb.css',
    'dist/css/button.css',
    'dist/css/card.css',
    'dist/css/card-carousel.css',
    'dist/css/global.css',
    'dist/css/gtranslate.css',
    'dist/css/heading.css',
    'dist/css/main-menu.css',
    'dist/css/search-box.css',
    'dist/css/site-footer.css',
    'dist/css/user-account-nav.css',
  ],

  // Files to copy.
  'copy' => [
    'logo.svg',
  ],
];
